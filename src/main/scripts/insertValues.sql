<?xml version="1.0" encoding="UTF-8"?>
<dataset>
    <AUTHORS AU_AUTHOR_ID_PK="1" AU_NAME="author1"/>
    <AUTHORS AU_AUTHOR_ID_PK="2" AU_NAME="author2"/>
    <AUTHORS AU_AUTHOR_ID_PK="3" AU_NAME="author3"/>
    <AUTHORS AU_AUTHOR_ID_PK="4" AU_NAME="author4"/>
    <AUTHORS AU_AUTHOR_ID_PK="5" AU_NAME="author5"/>
    <AUTHORS AU_AUTHOR_ID_PK="6" AU_NAME="author6"/>
    <AUTHORS AU_AUTHOR_ID_PK="7" AU_NAME="author7"/>
    <AUTHORS AU_AUTHOR_ID_PK="8" AU_NAME="author8"/>
    <AUTHORS AU_AUTHOR_ID_PK="9" AU_NAME="author9"/>
    <AUTHORS AU_AUTHOR_ID_PK="10" AU_NAME="author10"/>

    <NEWS NE_NEWS_ID_PK="1" NE_SHORT_TEXT="shortText1" NE_FULL_TEXT="fullText1" NE_TITLE="title1" NE_CREATION_DATE="2012-03-04 00:00:00.0" NE_MODIFICATION_DATE="2012-03-04 00:00:00.0"/>
    <NEWS NE_NEWS_ID_PK="2" NE_SHORT_TEXT="shortText2" NE_FULL_TEXT="fullText2" NE_TITLE="title2" NE_CREATION_DATE="1970-01-01 00:00:00.0" NE_MODIFICATION_DATE="1970-01-01 00:00:00.0"/>
    <NEWS NE_NEWS_ID_PK="3" NE_SHORT_TEXT="shortText3" NE_FULL_TEXT="fullText3" NE_TITLE="title3" NE_CREATION_DATE="2012-03-04 00:00:00.0" NE_MODIFICATION_DATE="2012-03-04 00:00:00.0"/>
    <NEWS NE_NEWS_ID_PK="4" NE_SHORT_TEXT="shortText4" NE_FULL_TEXT="fullText4" NE_TITLE="title4" NE_CREATION_DATE="2012-03-04 00:00:00.0" NE_MODIFICATION_DATE="2012-03-04 00:00:00.0"/>
    <NEWS NE_NEWS_ID_PK="5" NE_SHORT_TEXT="shortText5" NE_FULL_TEXT="fullText5" NE_TITLE="title5" NE_CREATION_DATE="2012-03-04 00:00:00.0" NE_MODIFICATION_DATE="2012-03-04 00:00:00.0"/>
    <NEWS NE_NEWS_ID_PK="6" NE_SHORT_TEXT="shortText6" NE_FULL_TEXT="fullText6" NE_TITLE="title6" NE_CREATION_DATE="2012-03-04 00:00:00.0" NE_MODIFICATION_DATE="2012-03-04 00:00:00.0"/>
    <NEWS NE_NEWS_ID_PK="7" NE_SHORT_TEXT="shortText7" NE_FULL_TEXT="fullText7" NE_TITLE="title7" NE_CREATION_DATE="2012-03-04 00:00:00.0" NE_MODIFICATION_DATE="2012-03-04 00:00:00.0"/>
    <NEWS NE_NEWS_ID_PK="8" NE_SHORT_TEXT="shortText8" NE_FULL_TEXT="fullText8" NE_TITLE="title8" NE_CREATION_DATE="2012-03-04 00:00:00.0" NE_MODIFICATION_DATE="2012-03-04 00:00:00.0"/>
    <NEWS NE_NEWS_ID_PK="9" NE_SHORT_TEXT="shortText9" NE_FULL_TEXT="fullText9" NE_TITLE="title9" NE_CREATION_DATE="2012-03-04 00:00:00.0" NE_MODIFICATION_DATE="2012-03-04 00:00:00.0"/>
    <NEWS NE_NEWS_ID_PK="10" NE_SHORT_TEXT="shortText10" NE_FULL_TEXT="fullText10" NE_TITLE="title10" NE_CREATION_DATE="2012-03-04 00:00:00.0" NE_MODIFICATION_DATE="2012-03-04 00:00:00.0"/>

    <TAGS TA_TAG_ID_PK="1" TA_TAG_NAME="tagName1"/>
    <TAGS TA_TAG_ID_PK="2" TA_TAG_NAME="tagName2"/>
    <TAGS TA_TAG_ID_PK="3" TA_TAG_NAME="tagName3"/>
    <TAGS TA_TAG_ID_PK="4" TA_TAG_NAME="tagName4"/>
    <TAGS TA_TAG_ID_PK="5" TA_TAG_NAME="tagName5"/>
    <TAGS TA_TAG_ID_PK="6" TA_TAG_NAME="tagName6"/>
    <TAGS TA_TAG_ID_PK="7" TA_TAG_NAME="tagName7"/>
    <TAGS TA_TAG_ID_PK="8" TA_TAG_NAME="tagName8"/>
    <TAGS TA_TAG_ID_PK="9" TA_TAG_NAME="tagName9"/>
    <TAGS TA_TAG_ID_PK="10" TA_TAG_NAME="tagName10"/>

    <COMMENTS CO_COMMENT_ID_PK="1" CO_COMMENT_TEXT="commentText1" CO_CREATION_DATE="2012-03-04 00:00:00.0" CO_NEWS_ID_FK="1"/>
    <COMMENTS CO_COMMENT_ID_PK="2" CO_COMMENT_TEXT="commentText2" CO_CREATION_DATE="1970-01-01 00:00:00.0" CO_NEWS_ID_FK="2"/>
    <COMMENTS CO_COMMENT_ID_PK="3" CO_COMMENT_TEXT="commentText3" CO_CREATION_DATE="2012-03-04 00:00:00.0" CO_NEWS_ID_FK="3"/>
    <COMMENTS CO_COMMENT_ID_PK="4" CO_COMMENT_TEXT="commentText4" CO_CREATION_DATE="2012-03-04 00:00:00.0" CO_NEWS_ID_FK="4"/>
    <COMMENTS CO_COMMENT_ID_PK="5" CO_COMMENT_TEXT="commentText5" CO_CREATION_DATE="2012-03-04 00:00:00.0" CO_NEWS_ID_FK="5"/>
    <COMMENTS CO_COMMENT_ID_PK="6" CO_COMMENT_TEXT="commentText6" CO_CREATION_DATE="2012-03-04 00:00:00.0" CO_NEWS_ID_FK="6"/>
    <COMMENTS CO_COMMENT_ID_PK="7" CO_COMMENT_TEXT="commentText7" CO_CREATION_DATE="2012-03-04 00:00:00.0" CO_NEWS_ID_FK="7"/>
    <COMMENTS CO_COMMENT_ID_PK="8" CO_COMMENT_TEXT="commentText8" CO_CREATION_DATE="2012-03-04 00:00:00.0" CO_NEWS_ID_FK="8"/>
    <COMMENTS CO_COMMENT_ID_PK="9" CO_COMMENT_TEXT="commentText9" CO_CREATION_DATE="2012-03-04 00:00:00.0" CO_NEWS_ID_FK="9"/>
    <COMMENTS CO_COMMENT_ID_PK="10" CO_COMMENT_TEXT="commentText10" CO_CREATION_DATE="2012-03-04 00:00:00.0" CO_NEWS_ID_FK="10"/>

    <NEWS_TAGS NT_NEWS_TAG_ID_PK="1" NT_NEWS_ID_FK="1" NT_TAG_ID_FK="1"/>
    <NEWS_TAGS NT_NEWS_TAG_ID_PK="2" NT_NEWS_ID_FK="2" NT_TAG_ID_FK="2"/>
    <NEWS_TAGS NT_NEWS_TAG_ID_PK="3" NT_NEWS_ID_FK="3" NT_TAG_ID_FK="3"/>
    <NEWS_TAGS NT_NEWS_TAG_ID_PK="4" NT_NEWS_ID_FK="4" NT_TAG_ID_FK="4"/>
    <NEWS_TAGS NT_NEWS_TAG_ID_PK="5" NT_NEWS_ID_FK="5" NT_TAG_ID_FK="5"/>
    <NEWS_TAGS NT_NEWS_TAG_ID_PK="6" NT_NEWS_ID_FK="6" NT_TAG_ID_FK="6"/>
    <NEWS_TAGS NT_NEWS_TAG_ID_PK="7" NT_NEWS_ID_FK="7" NT_TAG_ID_FK="7"/>
    <NEWS_TAGS NT_NEWS_TAG_ID_PK="8" NT_NEWS_ID_FK="8" NT_TAG_ID_FK="8"/>
    <NEWS_TAGS NT_NEWS_TAG_ID_PK="9" NT_NEWS_ID_FK="9" NT_TAG_ID_FK="9"/>
    <NEWS_TAGS NT_NEWS_TAG_ID_PK="10" NT_NEWS_ID_FK="10" NT_TAG_ID_FK="10"/>

    <NEWS_AUTHORS NA_NEWS_AUTHOR_ID_PK="1" NA_NEWS_ID_FK="1" NA_AUTHOR_ID_FK="1"/>
    <NEWS_AUTHORS NA_NEWS_AUTHOR_ID_PK="2" NA_NEWS_ID_FK="2" NA_AUTHOR_ID_FK="2"/>
    <NEWS_AUTHORS NA_NEWS_AUTHOR_ID_PK="3" NA_NEWS_ID_FK="3" NA_AUTHOR_ID_FK="3"/>
    <NEWS_AUTHORS NA_NEWS_AUTHOR_ID_PK="4" NA_NEWS_ID_FK="4" NA_AUTHOR_ID_FK="4"/>
    <NEWS_AUTHORS NA_NEWS_AUTHOR_ID_PK="5" NA_NEWS_ID_FK="5" NA_AUTHOR_ID_FK="5"/>
    <NEWS_AUTHORS NA_NEWS_AUTHOR_ID_PK="6" NA_NEWS_ID_FK="6" NA_AUTHOR_ID_FK="6"/>
    <NEWS_AUTHORS NA_NEWS_AUTHOR_ID_PK="7" NA_NEWS_ID_FK="7" NA_AUTHOR_ID_FK="7"/>
    <NEWS_AUTHORS NA_NEWS_AUTHOR_ID_PK="8" NA_NEWS_ID_FK="8" NA_AUTHOR_ID_FK="8"/>
    <NEWS_AUTHORS NA_NEWS_AUTHOR_ID_PK="9" NA_NEWS_ID_FK="9" NA_AUTHOR_ID_FK="9"/>
    <NEWS_AUTHORS NA_NEWS_AUTHOR_ID_PK="10" NA_NEWS_ID_FK="10" NA_AUTHOR_ID_FK="10"/>
</dataset>


INSERT INTO AUTHORS (AU_NAME) VALUES ('author1');
INSERT INTO AUTHORS (AU_NAME) VALUES ('author2');
INSERT INTO AUTHORS (AU_NAME) VALUES ('author3');
INSERT INTO AUTHORS (AU_NAME) VALUES ('author4');
INSERT INTO AUTHORS (AU_NAME) VALUES ('author5');
INSERT INTO AUTHORS (AU_NAME) VALUES ('author6');
INSERT INTO AUTHORS (AU_NAME) VALUES ('author7');
INSERT INTO AUTHORS (AU_NAME) VALUES ('author8');
INSERT INTO AUTHORS (AU_NAME) VALUES ('author9');
INSERT INTO AUTHORS (AU_NAME) VALUES ('author10');

INSERT INTO NEWS(NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE) VALUES ('fullText1','04-MAR-12','04-MAR-12','shortText1','title1');
INSERT INTO NEWS(NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE) VALUES ('fullText2', '01-JAN-70','01-JAN-70','shortText2','title2');
INSERT INTO NEWS(NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE) VALUES ('fullText3','04-MAR-12','04-MAR-12','shortText3','title3');
INSERT INTO NEWS(NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE) VALUES ('fullText4','04-MAR-12','04-MAR-12','shortText4','title4');
INSERT INTO NEWS(NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE) VALUES ('fullText5','04-MAR-12','04-MAR-12','shortText5','title5');
INSERT INTO NEWS(NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE) VALUES ('fullText6','04-MAR-12','04-MAR-12','shortText6','title6');
INSERT INTO NEWS(NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE) VALUES ('fullText7','04-MAR-12','04-MAR-12','shortText7','title7');
INSERT INTO NEWS(NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE) VALUES ('fullText8','04-MAR-12','04-MAR-12','shortText8','title8');
INSERT INTO NEWS(NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE) VALUES ('fullText9','04-MAR-12','04-MAR-12','shortText9','title9');
INSERT INTO NEWS(NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE) VALUES ('fullText10','04-MAR-12','04-MAR-12','shortText10','title10');

INSERT INTO TAGS(TA_TAG_NAME) VALUES('tagName1');
INSERT INTO TAGS(TA_TAG_NAME) VALUES('tagName2');
INSERT INTO TAGS(TA_TAG_NAME) VALUES('tagName3');
INSERT INTO TAGS(TA_TAG_NAME) VALUES('tagName4');
INSERT INTO TAGS(TA_TAG_NAME) VALUES('tagName5');
INSERT INTO TAGS(TA_TAG_NAME) VALUES('tagName6');
INSERT INTO TAGS(TA_TAG_NAME) VALUES('tagName7');
INSERT INTO TAGS(TA_TAG_NAME) VALUES('tagName8');
INSERT INTO TAGS(TA_TAG_NAME) VALUES('tagName9');
INSERT INTO TAGS(TA_TAG_NAME) VALUES('tagName10');

INSERT INTO COMMENTS (CO_COMMENT_TEXT,CO_CREATION_DATE,CO_NEWS_ID_FK) VALUES('commentText1','04-MAR-12', 1);
INSERT INTO COMMENTS (CO_COMMENT_TEXT,CO_CREATION_DATE,CO_NEWS_ID_FK) VALUES('commentText2','01-JAN-70', 2);
INSERT INTO COMMENTS (CO_COMMENT_TEXT,CO_CREATION_DATE,CO_NEWS_ID_FK) VALUES('commentText3','04-MAR-12', 3);
INSERT INTO COMMENTS (CO_COMMENT_TEXT,CO_CREATION_DATE,CO_NEWS_ID_FK) VALUES('commentText4','04-MAR-12', 4);
INSERT INTO COMMENTS (CO_COMMENT_TEXT,CO_CREATION_DATE,CO_NEWS_ID_FK) VALUES('commentText5','04-MAR-12', 5);
INSERT INTO COMMENTS (CO_COMMENT_TEXT,CO_CREATION_DATE,CO_NEWS_ID_FK) VALUES('commentText6','04-MAR-12', 6);
INSERT INTO COMMENTS (CO_COMMENT_TEXT,CO_CREATION_DATE,CO_NEWS_ID_FK) VALUES('commentText7','04-MAR-12', 7);
INSERT INTO COMMENTS (CO_COMMENT_TEXT,CO_CREATION_DATE,CO_NEWS_ID_FK) VALUES('commentText8','04-MAR-12', 8);
INSERT INTO COMMENTS (CO_COMMENT_TEXT,CO_CREATION_DATE,CO_NEWS_ID_FK) VALUES('commentText9','04-MAR-12', 9);
INSERT INTO COMMENTS (CO_COMMENT_TEXT,CO_CREATION_DATE,CO_NEWS_ID_FK) VALUES('commentText10','04-MAR-12', 10);

INSERT INTO NEWS_AUTHORS(NA_AUTHOR_ID_FK,NA_NEWS_ID_FK) VALUES(1,1);
INSERT INTO NEWS_AUTHORS(NA_AUTHOR_ID_FK,NA_NEWS_ID_FK) VALUES(2,2);
INSERT INTO NEWS_AUTHORS(NA_AUTHOR_ID_FK,NA_NEWS_ID_FK) VALUES(3,3);
INSERT INTO NEWS_AUTHORS(NA_AUTHOR_ID_FK,NA_NEWS_ID_FK) VALUES(4,4);
INSERT INTO NEWS_AUTHORS(NA_AUTHOR_ID_FK,NA_NEWS_ID_FK) VALUES(5,5);
INSERT INTO NEWS_AUTHORS(NA_AUTHOR_ID_FK,NA_NEWS_ID_FK) VALUES(6,6);
INSERT INTO NEWS_AUTHORS(NA_AUTHOR_ID_FK,NA_NEWS_ID_FK) VALUES(7,7);
INSERT INTO NEWS_AUTHORS(NA_AUTHOR_ID_FK,NA_NEWS_ID_FK) VALUES(8,8);
INSERT INTO NEWS_AUTHORS(NA_AUTHOR_ID_FK,NA_NEWS_ID_FK) VALUES(9,9);
INSERT INTO NEWS_AUTHORS(NA_AUTHOR_ID_FK,NA_NEWS_ID_FK) VALUES(10,10);

INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(1,1);
INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(2,2);
INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(3,3);
INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(4,4);
INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(5,5);
INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(6,6);
INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(7,7);
INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(8,8);
INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(9,9);
INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(10,10);

--INSERT INTO AUTHORS (AU_NAME) VALUES ('Petrov');
--INSERT INTO AUTHORS (AU_NAME) VALUES ('Ivanov');
--INSERT INTO AUTHORS (AU_NAME) VALUES ('Sidorov');
--INSERT INTO AUTHORS (AU_NAME) VALUES ('Bondarev');
--INSERT INTO AUTHORS (AU_NAME) VALUES ('Saponenko');
--INSERT INTO AUTHORS (AU_NAME) VALUES ('Burinskaya');
--
--INSERT INTO NEWS(NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE) VALUES ('The mummified body of a 90-year-old woman, who is believed to have died years ago, was exhumed by firefighters Saturday night from a rat-infested San Francisco home that’s literally filled to the ceiling with trash.
--Workers wearing oxygen masks needed several days to remove enough of the trash to uncover the corpse of the unidentified woman, CBS San Francisco reported.
--Stranger still, the hoarder had company: Her 65-year-old daughter lived in the house and knew about the body for years.
--“I’m saddened that she couldn’t let her mother go,” neighbor Sharon Scott Kish told CBS San Francisco. “I saw her three days ago and I asked her, ‘Is your mother still with us?’ And she said, ‘Yes.’ Delusional. She was with her.',TO_DATE('06-APR-2015','DD-MON-YYYY'),TO_DATE('06-APR-2015','DD-MON-YYYY'),'The mummified body of a 90-year-old woman','Mummified body');
--INSERT INTO NEWS(NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE) VALUES ('The UN Security Council has demanded humanitarian access to the Palestinian refugee camp of Yarmouk in Damascus.
--One UN official described the situation for the 18,000 refugees there as "beyond inhumane".
--The situation has deteriorated since 1 April, when Islamic State launched an offensive.
--Palestinian militiamen opposed to the Syrian government and some Free Syrian Army fighters are leading the fight against the IS militants.',TO_DATE('03-APR-2015','DD-MON-YYYY'),TO_DATE('04-APR-2015','DD-MON-YYYY'),'The UN Security Council has demanded humanitarian access.','UN demands access');
--INSERT INTO NEWS(NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE) VALUES ('Malaysia has passed a controversial anti-terrorism bill, which the government says is needed to tackle the threat from Islamic extremists.
--The bill reintroduces indefinite detention without trial - something the prime minister had repealed in 2012.
--Human Rights Watch called the move "a giant step backwards for human rights".
--It was passed hours after the police announced the detention of 17 suspected militants believed to be planning attacks in the capital, Kuala Lumpur.
--Home Minister Zahid Hamidi said those arrested, the youngest just 14, were planning to attack police stations and army bases to gather weapons.
--Two of the suspects had just returned from Syria, police said.',TO_DATE('10-MAR-2015','DD-MON-YYYY'),TO_DATE('12-MAR-2015','DD-MON-YYYY'),'Malaysia has passed a controversial anti-terrorism bill.','Malaysia passes contro');
--INSERT INTO NEWS(NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE) VALUES ('An explosion has ripped through a chemical plant in south-eastern Chinas Fujian province, sparking a major fire.
--The blast occurred on Monday evening at the plant in Zhangzhou. State news agency Xinhua reported three injuries.The plant produces paraxylene (PX), a flammable chemical used in polyester and plastics manufacturing.
--Construction of PX plants in China has sparked protests from residents, including violent demonstrations last year in Guangdong province.
--Many residents fear pollution from the plants is hazardous to health.',TO_DATE('10-MAR-2015','DD-MON-YYYY'),TO_DATE('11-MAR-2015','DD-MON-YYYY'),'An explosion has ripped through a chemical plant.','China paraxylene chemical');
--INSERT INTO NEWS(NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE) VALUES ('As the original manuscript for Don Mcleans 1971 classic goes up for auction, fans may finally discover what the "Song of the Century" is really about. So what are the popular theories?
--When people ask Don McLean what does American Pie really mean, he likes to reply: "It means I never have to work again."
--His eight-minute-long "rock and roll American dream" became an anthem for an entire generation - who memorised every line.
--Their children in turn grew up singing it - fascinated by the mysterious lyrics with their cryptic references to 50s innocence, the turbulent 60s, and 70s disillusion.',TO_DATE('10-AUG-2014','DD-MON-YYYY'),TO_DATE('02-MAR-2015','DD-MON-YYYY'),'As the original manuscript for Don Mcleans 1971 classic goes up for auction','What do lyrics mean?');
--INSERT INTO NEWS(NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE) VALUES ('Every year, on the evening before New Years Eve, thousands of people in New York City amble through the neon glow of Times Square.
--Most are tourists looking up in awe at the LED screens that dominate the landscape. Nobody notices the tiny dark figure 350 feet above the pavement clinging to the side of the century old Times Building, home of the New Years Eve Ball.
--Jeff Rolfe is a cross between Spider Man and Guy Fawkes, gingerly making his way up and down the side of the building with a bitter winter wind attacking him from every angle as he dangles.',TO_DATE('13-OCT-2015','DD-MON-YYYY'),TO_DATE('15-OCT-2015','DD-MON-YYYY'),'Every year, on the evening before New Years Eve.','How tech made fireworks');
--INSERT INTO NEWS(NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE) VALUES ('California is facing a catastrophic environmental disaster. Americas erstwhile Golden State is in the midst of a severe drought which shows no sign of letting up.
--Even the threat of earthquakes seems to fade in comparison to the water crisis, now in its fourth year.
--Nasa scientists have projected that reservoirs could run dry within a year and there is growing pressure on ground water supplies, which are dwindling rapidly.
--The drought is a problem of epic proportions and it could - many say should - result in a seismic shift in attitudes towards water.
--"Hopefully people will look at a green lawn and find it as annoying as second-hand cigarette smoke," says Nancy Vogel of the California Department of Water Resources.',TO_DATE('13-JAN-2015','DD-MON-YYYY'),TO_DATE('10-FEB-2015','DD-MON-YYYY'),'California Governor Jerry Brown has instituted new water restrictions','Will State turn brown?');
--INSERT INTO NEWS(NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE) VALUES ('"Training very hard as bad as no exercise at all," reported the BBC. "Fast running is as deadly as sitting on couch," agreed the Daily Telegraph and countless other newspapers around the world, striking fear into the hearts of hardcore runners.
--The headlines were based on the results of a large Danish study - more than 1,000 healthy joggers and almost 4,000 healthy non-joggers were followed up over 12 years, as part of the Copenhagen City Heart Study.',TO_DATE('10-FEB-2015','DD-MON-YYYY'),TO_DATE('12-FEB-2015','DD-MON-YYYY'),'A recent study reported that joggers who exercise strenuously have the same life.','Why runs may not be so bad');
--
--INSERT INTO TAGS(TA_TAG_NAME) VALUES('Shock');
--INSERT INTO TAGS(TA_TAG_NAME) VALUES('Event');
--INSERT INTO TAGS(TA_TAG_NAME) VALUES('Fun');
--INSERT INTO TAGS(TA_TAG_NAME) VALUES('Sport');
--INSERT INTO TAGS(TA_TAG_NAME) VALUES('Dance');
--INSERT INTO TAGS(TA_TAG_NAME) VALUES('BBC');
--INSERT INTO TAGS(TA_TAG_NAME) VALUES('Europe');
--INSERT INTO TAGS(TA_TAG_NAME) VALUES('USA');
--INSERT INTO TAGS(TA_TAG_NAME) VALUES('Politics');
--INSERT INTO TAGS(TA_TAG_NAME) VALUES('Weather');
--
--INSERT INTO COMMENTS (CO_COMMENT_TEXT,CO_CREATION_DATE,CO_NEWS_ID_FK) VALUES('Amazing',TO_DATE('11-MAR-2015','DD-MON-YYYY'),3);
--INSERT INTO COMMENTS (CO_COMMENT_TEXT,CO_CREATION_DATE,CO_NEWS_ID_FK) VALUES('Really interesting news',TO_DATE('05-APR-2015','DD-MON-YYYY'),4);
--INSERT INTO COMMENTS (CO_COMMENT_TEXT,CO_CREATION_DATE,CO_NEWS_ID_FK) VALUES('Congratulations',TO_DATE('10-MAR-2015','DD-MON-YYYY'),4);
--INSERT INTO COMMENTS (CO_COMMENT_TEXT,CO_CREATION_DATE,CO_NEWS_ID_FK) VALUES('Fun',TO_DATE('10-FEB-2015','DD-MON-YYYY'),8);
--INSERT INTO COMMENTS (CO_COMMENT_TEXT,CO_CREATION_DATE,CO_NEWS_ID_FK) VALUES('I am not interested in this',TO_DATE('11-MAR-2015','DD-MON-YYYY'),5);
--
--INSERT INTO NEWS_AUTHORS(NA_AUTHOR_ID_FK,NA_NEWS_ID_FK) VALUES(1,1);
--INSERT INTO NEWS_AUTHORS(NA_AUTHOR_ID_FK,NA_NEWS_ID_FK) VALUES(1,2);
--INSERT INTO NEWS_AUTHORS(NA_AUTHOR_ID_FK,NA_NEWS_ID_FK) VALUES(2,3);
--INSERT INTO NEWS_AUTHORS(NA_AUTHOR_ID_FK,NA_NEWS_ID_FK) VALUES(3,4);
--INSERT INTO NEWS_AUTHORS(NA_AUTHOR_ID_FK,NA_NEWS_ID_FK) VALUES(4,5);
--INSERT INTO NEWS_AUTHORS(NA_AUTHOR_ID_FK,NA_NEWS_ID_FK) VALUES(5,6);
--INSERT INTO NEWS_AUTHORS(NA_AUTHOR_ID_FK,NA_NEWS_ID_FK) VALUES(6,7);
--INSERT INTO NEWS_AUTHORS(NA_AUTHOR_ID_FK,NA_NEWS_ID_FK) VALUES(6,8);
--
--INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(1,1);
--INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(2,2);
--INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(2,3);
--INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(3,4);
--INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(3,5);
--INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(4,6);
--INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(4,7);
--INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(5,8);
--INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(6,9);
--INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(7,10);
--INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(8,11);
--INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(6,6);
--INSERT INTO NEWS_TAGS(NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(6,4);
