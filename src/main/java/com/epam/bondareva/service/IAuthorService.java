package com.epam.bondareva.service;

import com.epam.bondareva.entity.Author;
import com.epam.bondareva.exception.ServiceException;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
public interface IAuthorService {
    long addAuthor(Author author) throws ServiceException;

    void addAuthorNews(Long authorId, Long newsId) throws ServiceException;

    void deleteAuthorNews(Long authorId, Long newsId) throws ServiceException;
}
