package com.epam.bondareva.service;

import com.epam.bondareva.entity.News;
import com.epam.bondareva.entity.Tag;
import com.epam.bondareva.exception.ServiceException;

/**
 * Created by Olya Bondareva on 04/04/2015.
 */
public interface INewsManagementService {
    void addTagToNews(Long newsId, Tag tag) throws ServiceException;

    void addNewsToAuthor(Long authorId, News news) throws ServiceException;

    void deleteNewsFromAuthor(Long authorId, Long newsId) throws ServiceException;
}
