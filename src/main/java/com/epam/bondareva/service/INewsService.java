package com.epam.bondareva.service;

import com.epam.bondareva.entity.Author;
import com.epam.bondareva.entity.News;
import com.epam.bondareva.entity.Tag;
import com.epam.bondareva.exception.ServiceException;

import java.util.List;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
public interface INewsService {
    Long addNews(News news) throws ServiceException;

    void updateNews(News news) throws ServiceException;

    void deleteNews(Long newsId) throws ServiceException;

    List<News> viewNewsList() throws ServiceException;

    News viewSingleNews(Long newsId) throws ServiceException;

    List<News> searchByTag(Tag tag) throws ServiceException;

    List<News> searchByAuthor(Author author) throws ServiceException;

    void addNewsTag(Long newsId, Long tagId) throws ServiceException;

    void deleteNewsTag(Long newsId, Long tagId) throws ServiceException;
}
