package com.epam.bondareva.service.implementation;

import com.epam.bondareva.dao.ITagDao;
import com.epam.bondareva.entity.Tag;
import com.epam.bondareva.exception.DaoException;
import com.epam.bondareva.exception.ServiceException;
import com.epam.bondareva.service.ITagService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
@Component
public class TagService implements ITagService {
    private static Logger logger = Logger.getLogger(TagService.class);
    @Autowired
    private ITagDao iTagDao;

    public TagService() {
    }

    public TagService(ITagDao iTagDao) {
        this.iTagDao = iTagDao;
    }

    @Override
    public long addTag(Tag tag) throws ServiceException {
        try {
            return iTagDao.createEntity(tag);
        } catch (DaoException e) {
            logger.error("Error during creating tag",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteTag(Long tagId) throws ServiceException {
        Tag tagFromDatabase;
        try {
            tagFromDatabase = iTagDao.getEntityById(tagId);
            if (tagFromDatabase != null) {
                iTagDao.deleteEntity(tagId);
            }
            else {
                throw new ServiceException("no such tag in storage");
            }
        } catch (DaoException e) {
            logger.error("Error during deleting tag",e);
            throw new ServiceException(e);
        }

    }

    @Override
    public List<Tag> getTagListByNewsId(Long newsId) throws ServiceException {
        try {
            return iTagDao.getEntityListByNewsId(newsId);
        } catch (DaoException e) {
            logger.error("Error during getting tag by news id",e);
            throw new ServiceException(e);
        }
    }
}
