package com.epam.bondareva.service.implementation;

import com.epam.bondareva.dao.IAuthorDao;
import com.epam.bondareva.entity.Author;
import com.epam.bondareva.exception.DaoException;
import com.epam.bondareva.exception.ServiceException;
import com.epam.bondareva.service.IAuthorService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
@Component
public class AuthorService implements IAuthorService {
    private static Logger logger = Logger.getLogger(AuthorService.class);
    @Autowired
    private IAuthorDao iAuthorDao;

    public AuthorService() {
    }

    public AuthorService(IAuthorDao iAuthorDao) {
        this.iAuthorDao = iAuthorDao;
    }

    @Override
    public long addAuthor(Author author) throws ServiceException {
        try {
            return iAuthorDao.createEntity(author);
        } catch (DaoException e) {
            logger.error("Error during creating author", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void addAuthorNews(Long authorId, Long newsId) throws ServiceException {
        try {
            iAuthorDao.addAuthorNews(authorId, newsId);
        } catch (DaoException e) {
            logger.error("Error during addition news to author", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteAuthorNews(Long authorId, Long newsId) throws ServiceException {
        try {
            iAuthorDao.deleteAuthorNews(authorId, newsId);
        } catch (DaoException e) {
            logger.error("Error during deleting news from author", e);
            throw new ServiceException(e);
        }
    }

}
