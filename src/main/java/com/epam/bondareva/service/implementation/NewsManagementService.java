package com.epam.bondareva.service.implementation;

import com.epam.bondareva.dao.ICommentDao;
import com.epam.bondareva.dao.ITagDao;
import com.epam.bondareva.entity.Comment;
import com.epam.bondareva.entity.News;
import com.epam.bondareva.entity.NewsDTO;
import com.epam.bondareva.entity.Tag;
import com.epam.bondareva.exception.ServiceException;
import com.epam.bondareva.service.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * Created by Olya Bondareva on 04/04/2015.
 */
@Component
public class NewsManagementService implements INewsManagementService {
    private static Logger logger = Logger.getLogger(NewsManagementService.class);
    @Autowired
    private INewsService iNewsService;
    @Autowired
    private ITagService iTagService;
    @Autowired
    private IAuthorService iAuthorService;
    @Autowired
    private ICommentService iCommentService;

    @Autowired
    private ITagDao iTagDao;
    @Autowired
    private ICommentDao iCommentDao;


    public NewsManagementService() {
    }

    public INewsService getINewsService() {
        return iNewsService;
    }

    public void setINewsService(INewsService iNewsService) {
        this.iNewsService = iNewsService;
    }

    public ITagService getITagService() {
        return iTagService;
    }

    public void setITagService(ITagService iTagService) {
        this.iTagService = iTagService;
    }

    public IAuthorService getIAuthorService() {
        return iAuthorService;
    }

    public void setIAuthorService(IAuthorService iAuthorService) {
        this.iAuthorService = iAuthorService;
    }

    public ICommentService getICommentService() {
        return iCommentService;
    }

    public void setICommentService(ICommentService iCommentService) {
        this.iCommentService = iCommentService;
    }

    @Override
    public void addTagToNews(Long newsId, Tag tag) throws ServiceException {
        try {
            long tagId = iTagService.addTag(tag);
            iNewsService.addNewsTag(newsId, tagId);
        } catch (ServiceException e) {
            logger.error("Error during adding tag to author", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void addNewsToAuthor(Long authorId, News news) throws ServiceException {
        try {
            long newsId = iNewsService.addNews(news);
            iAuthorService.addAuthorNews(authorId, newsId);
        } catch (ServiceException e) {
            logger.error("Error during adding news to author", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteNewsFromAuthor(Long authorId, Long newsId) throws ServiceException {
        try {
            NewsDTO newsDTO = new NewsDTO();
            newsDTO.setTagList(iTagService.getTagListByNewsId(newsId));
            for (Tag tag : newsDTO.getTagList()) {
                iNewsService.deleteNewsTag(newsId, tag.getId());
            }
            newsDTO = new NewsDTO();
            newsDTO.setCommentList(iCommentService.getCommentListByNewsId(newsId));
            for (Comment comment : newsDTO.getCommentList()) {
                iCommentService.deleteComment(comment.getId());
                iCommentService.deleteNewsComment(newsId, comment.getId());
            }
            iAuthorService.deleteAuthorNews(authorId, newsId);
            iNewsService.deleteNews(newsId);
        } catch (ServiceException e) {
            logger.error("Error during deleting news", e);
            throw new ServiceException(e);
        }
    }


    //todo: метод который возвращает заполненный дто

}
