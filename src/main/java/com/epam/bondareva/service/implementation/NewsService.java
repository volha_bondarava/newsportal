package com.epam.bondareva.service.implementation;

import com.epam.bondareva.dao.INewsDao;
import com.epam.bondareva.entity.Author;
import com.epam.bondareva.entity.News;
import com.epam.bondareva.entity.Tag;
import com.epam.bondareva.exception.DaoException;
import com.epam.bondareva.exception.ServiceException;
import com.epam.bondareva.service.INewsService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
@Component
public class NewsService implements INewsService {
    private static Logger logger = Logger.getLogger(NewsService.class);
    @Autowired
    private INewsDao iNewsDao;

    public NewsService() {
    }

    @Autowired
    public NewsService(INewsDao iNewsDao) {
        this.iNewsDao = iNewsDao;
    }

    @Override
    public Long addNews(News news) throws ServiceException{
        try {
            return iNewsDao.createEntity(news);
        } catch (DaoException e) {
            logger.error("Error during creating news", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void updateNews(News news) throws ServiceException {
        News newsFromDatabase;
        try {
            newsFromDatabase = iNewsDao.getEntityById(news.getId());
            if (newsFromDatabase != null) {
                iNewsDao.updateEntity(news);
            }
            else {
                logger.error("no such news in storage");
                throw new ServiceException("no such news in storage");
            }
        } catch (DaoException e) {
            logger.error("Error during updating news",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteNews(Long newsId) throws ServiceException {
        News newsFromDatabase;
        try {
            newsFromDatabase = iNewsDao.getEntityById(newsId);
            if (newsFromDatabase != null) {
                iNewsDao.deleteEntity(newsId);
            }
            else {
                throw new ServiceException("no such news in storage");
            }
        } catch (DaoException e) {
            logger.error("Error during deleting news",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> viewNewsList() throws ServiceException {
        try {
            return iNewsDao.getEntityList();
        } catch (DaoException e) {
            logger.error("Error during getting the list of news",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public News viewSingleNews(Long newsId) throws ServiceException {
        try {
            return iNewsDao.getEntityById(newsId);
        } catch (DaoException e) {
            logger.error("Error during getting single news",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> searchByTag(Tag tag) throws ServiceException {
        try {
            return iNewsDao.searchByTag(tag);
        } catch (DaoException e) {
            logger.error("Error during searching by tag",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> searchByAuthor(Author author) throws ServiceException {
        try {
            return iNewsDao.searchByAuthor(author);
        } catch (DaoException e) {
            logger.error("Error during searching by author",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void addNewsTag(Long newsId, Long tagId) throws ServiceException {
        try {
            iNewsDao.addNewsTag(newsId, tagId);
        } catch (DaoException e) {
            logger.error("Error during adding tag to news",e);
            throw new ServiceException(e);
        }
    }
    @Override
    public void deleteNewsTag(Long newsId, Long tagId) throws ServiceException {
        try {
            iNewsDao.deleteNewsTag(newsId, tagId);
        } catch (DaoException e) {
            logger.error("Error during deleting tag from news",e);
            throw new ServiceException(e);
        }
    }
}
