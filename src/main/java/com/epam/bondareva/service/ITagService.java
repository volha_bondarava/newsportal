package com.epam.bondareva.service;

import com.epam.bondareva.entity.Comment;
import com.epam.bondareva.entity.Tag;
import com.epam.bondareva.exception.ServiceException;

import java.util.List;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
public interface ITagService {
    long addTag(Tag tag) throws ServiceException;

    void deleteTag(Long tagId) throws ServiceException;

    List<Tag> getTagListByNewsId(Long newsId) throws ServiceException;
}
