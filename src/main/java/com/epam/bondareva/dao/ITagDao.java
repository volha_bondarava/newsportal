package com.epam.bondareva.dao;

import com.epam.bondareva.entity.Tag;
import com.epam.bondareva.exception.DaoException;

import java.util.List;

/**
 * Created by Olya Bondareva on 29/03/2015.
 */
public interface ITagDao extends IDao<Tag>{
    List<Tag> getEntityListByNewsId(Long newsId) throws DaoException;
}
