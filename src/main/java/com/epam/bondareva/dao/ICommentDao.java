package com.epam.bondareva.dao;

import com.epam.bondareva.entity.Comment;
import com.epam.bondareva.exception.DaoException;

import java.util.List;

/**
 * Created by Olya Bondareva on 29/03/2015.
 */
public interface ICommentDao extends IDao<Comment>{
    void deleteNewsComment(Long newsId, Long commentId) throws DaoException;
    List<Comment> getEntityListByNewsId(Long newsId) throws DaoException;
}
