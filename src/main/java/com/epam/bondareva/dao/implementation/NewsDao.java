package com.epam.bondareva.dao.implementation;

import com.epam.bondareva.dao.INewsDao;
import com.epam.bondareva.entity.Author;
import com.epam.bondareva.entity.News;
import com.epam.bondareva.entity.Tag;
import com.epam.bondareva.exception.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
@Component
public class NewsDao implements INewsDao {
    private String INSERT_INTO_NEWS = "INSERT INTO NEWS(NE_NEWS_ID_PK,NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE) VALUES (SEQ_NEWS.NEXTVAL,?,?,?,?,?)";
    private String INSERT_INTO_NEWS_TAGS = "INSERT INTO NEWS_TAGS(NT_NEWS_ID_PK,NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(SEQ_NEWS_TAG.NEXTVAL,?,?)";
    private String DELETE_FROM_NEWS_TAGS = "DELETE FROM NEWS_TAGS WHERE NT_NEWS_ID_FK = ? AND NT_TAG_ID_FK=?";
    private String DELETE_NEWS = "DELETE FROM NEWS WHERE NE_NEWS_ID_PK = ?";
    private String SELECT_NEWS_BY_ID = "SELECT NE_NEWS_ID_PK, NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE FROM NEWS WHERE NE_NEWS_ID_PK = ?";
    private String SELECT_IN_ORDER_OF_MOST_COMMENTED_NEWS = "SELECT NE_NEWS_ID_PK,NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE\n" +
            "FROM NEWS INNER JOIN COMMENTS ON NEWS.NE_NEWS_ID_PK=COMMENTS.CO_NEWS_ID_FK\n" +
            "GROUP BY NE_NEWS_ID_PK,NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE ORDER BY COUNT(CO_NEWS_ID_FK) DESC";
    private String SELECT_NEWS_BY_TAG = "SELECT NE_NEWS_ID_PK,NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE \n" +
            "FROM NEWS INNER JOIN NEWS_TAGS ON NEWS.NE_NEWS_ID_PK = NEWS_TAGS.NT_NEWS_ID_FK WHERE NT_TAG_ID_FK=?";
    private String SELECT_NEWS_BY_AUTHOR = "SELECT NE_NEWS_ID_PK,NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE \n" +
            "FROM NEWS INNER JOIN NEWS_AUTHORS ON NEWS.NE_NEWS_ID_PK = NEWS_AUTHORS.NA_NEWS_ID_FK WHERE NA_AUTHOR_ID_FK=?";
    private String UPDATE_NEWS = "UPDATE NEWS SET NE_FULL_TEXT=?,NE_MODIFICATION_DATE=?,NE_SHORT_TEXT=?,NE_TITLE =? WHERE NE_NEWS_ID_PK=?";

    @Autowired
    private DataSource dataSource;

    @Override
    public Long createEntity(News news) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (
                PreparedStatement preparedStatement =
                        connection.prepareStatement(INSERT_INTO_NEWS, new String[]{"NE_NEWS_ID_PK"});
        ) {
            Long newsId = null;

            preparedStatement.setString(1, news.getFullText());
            preparedStatement.setObject(2, news.getCreationDate());
            preparedStatement.setObject(3, news.getModificationDate());
            preparedStatement.setString(4, news.getShortText());
            preparedStatement.setString(5, news.getTitle());

            if (preparedStatement.executeUpdate() > 0) {
                ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
                if (generatedKeys != null && generatedKeys.next()) {
                    newsId = generatedKeys.getLong(1);
                }
            }
            return newsId;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public NewsDao() {
    }

    @Override
    public void updateEntity(News news) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_NEWS)
        ) {
            preparedStatement.setString(1, news.getFullText());
            preparedStatement.setObject(2, news.getModificationDate());
            preparedStatement.setString(3, news.getShortText());
            preparedStatement.setString(4, news.getTitle());
            preparedStatement.setLong(5, news.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteEntity(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement1 =
                     connection.prepareStatement(DELETE_NEWS)
        ) {
            preparedStatement1.setLong(1, newsId);
            preparedStatement1.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public News getEntityById(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_NEWS_BY_ID)) {
            preparedStatement.setLong(1, newsId);
            ResultSet resultSet = preparedStatement.executeQuery();
            News news = new News();
            if (resultSet.next()) {
                news.setId(resultSet.getLong(1));
                news.setFullText(resultSet.getString(2));
                news.setCreationDate(resultSet.getTimestamp(3));
                news.setModificationDate(resultSet.getTimestamp(4));
                news.setShortText(resultSet.getString(5));
                news.setTitle(resultSet.getString(6));
            }
            return news;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> getEntityList() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (Statement statement =
                     connection.createStatement()
        ) {
            ResultSet resultSet = statement.executeQuery(SELECT_IN_ORDER_OF_MOST_COMMENTED_NEWS);
            List<News> newsList = new ArrayList<>();
            parse(resultSet, newsList);
            return newsList;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public List<News> searchByTag(Tag tag) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_NEWS_BY_TAG)
        ) {
            preparedStatement.setLong(1, tag.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            List<News> newsList = new ArrayList<>();
            parse(resultSet, newsList);
            return newsList;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public List<News> searchByAuthor(Author author) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_NEWS_BY_AUTHOR)
        ) {
            preparedStatement.setLong(1, author.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            List<News> newsList = new ArrayList<>();
            parse(resultSet, newsList);
            return newsList;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void addNewsTag(Long newsId, Long tagId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (
                PreparedStatement preparedStatement =
                        connection.prepareStatement(INSERT_INTO_NEWS_TAGS)
        ) {
            preparedStatement.setLong(1, newsId);
            preparedStatement.setLong(2, tagId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteNewsTag(Long newsId, Long tagId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (
                PreparedStatement preparedStatement =
                        connection.prepareStatement(DELETE_FROM_NEWS_TAGS)
        ) {
            preparedStatement.setLong(1, newsId);
            preparedStatement.setLong(2, tagId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    private void parse(ResultSet resultSet, List<News> newsList) throws SQLException {
        while (resultSet.next()) {
            News news = new News();
            news.setId(resultSet.getLong(1));
            news.setFullText(resultSet.getString(2));
            news.setCreationDate(resultSet.getTimestamp(3));
            news.setModificationDate(resultSet.getTimestamp(4));
            news.setShortText(resultSet.getString(5));
            news.setTitle(resultSet.getString(6));
            newsList.add(news);
        }
    }

}
