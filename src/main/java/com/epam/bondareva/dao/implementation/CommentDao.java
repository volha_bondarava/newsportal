package com.epam.bondareva.dao.implementation;

import com.epam.bondareva.dao.ICommentDao;
import com.epam.bondareva.entity.Comment;
import com.epam.bondareva.exception.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
@Component
public class CommentDao implements ICommentDao {
    private String DELETE_COMMENT = "DELETE FROM COMMENTS WHERE CO_COMMENT_ID_PK = ?";
    private String SELECT_COMMENTS_BY_NEWS_ID = "SELECT CO_COMMENT_ID_PK, CO_COMMENT_TEXT, CO_CREATION_DATE, CO_NEWS_ID_FK FROM COMMENTS WHERE CO_NEWS_ID_FK = ?";
    private String SELECT_COMMENT_BY_ID = "SELECT CO_COMMENT_ID_PK, CO_COMMENT_TEXT, CO_CREATION_DATE, CO_NEWS_ID_FK FROM COMMENTS WHERE CO_COMMENT_ID_PK = ?";
    private String UPDATE_COMMENT = "UPDATE COMMENTS SET CO_COMMENT_TEXT = ? WHERE CO_COMMENT_ID_PK = ?";
    private String INSERT_INTO_COMMENTS = "INSERT INTO COMMENTS (CO_COMMENT_ID_PK, CO_COMMENT_TEXT,CO_CREATION_DATE,CO_NEWS_ID_FK) VALUES(SEQ_COMMENT.NEXTVAL, ?,?,?)";
    private String DELETE_NEWS_FROM_COMMENTS = "DELETE FROM COMMENTS WHERE CO_NEWS_ID_FK=? AND CO_COMMENT_ID_PK=?";
    private String SELECT_COMMENT_LIST = "SELECT CO_COMMENT_ID_PK, CO_COMMENT_TEXT, CO_CREATION_DATE, CO_NEWS_ID_FK FROM COMMENTS";
    @Autowired
    private DataSource dataSource;

    public CommentDao() {
    }

    @Override
    public Long createEntity(Comment comment) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        Long commentId = null;
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(INSERT_INTO_COMMENTS, new String[]{"CO_COMMENT_ID_PK"})
        ) {
            preparedStatement.setString(1, comment.getText());
            preparedStatement.setObject(2, comment.getCreationDate());
            preparedStatement.setLong(3, comment.getNewsId());

            if (preparedStatement.executeUpdate() > 0) {
                ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
                if (generatedKeys != null && generatedKeys.next()) {
                    commentId = generatedKeys.getLong(1);
                }
            }
            return commentId;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void updateEntity(Comment comment) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(UPDATE_COMMENT)
        ) {
            preparedStatement.setString(1, comment.getText());
            preparedStatement.setLong(2, comment.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteEntity(Long commentId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(DELETE_COMMENT)
        ) {
            preparedStatement.setLong(1, commentId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Comment getEntityById(Long commentId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_COMMENT_BY_ID)
        ) {
            preparedStatement.setLong(1, commentId);
            ResultSet resultSet = preparedStatement.executeQuery();
            Comment comment = null;
            if (resultSet.next()) {
                comment = new Comment();
                comment.setId(resultSet.getLong(1));
                comment.setText(resultSet.getString(2));
                comment.setCreationDate(resultSet.getTimestamp(3));
                comment.setNewsId(resultSet.getLong(4));
            }
            return comment;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Comment> getEntityList() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement1 =
                     connection.prepareStatement(SELECT_COMMENT_LIST)
        ) {
            ResultSet resultSet = preparedStatement1.executeQuery();
            List<Comment> commentList = new ArrayList<>();
            while (resultSet.next()) {
                Comment comment = new Comment();
                comment.setId(resultSet.getLong(1));
                comment.setText(resultSet.getString(2));
                comment.setCreationDate(resultSet.getTimestamp(3));
                commentList.add(comment);
            }
            return commentList;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public List<Comment> getEntityListByNewsId(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement1 =
                     connection.prepareStatement(SELECT_COMMENTS_BY_NEWS_ID)
        ) {
            preparedStatement1.setLong(1, newsId);
            ResultSet resultSet = preparedStatement1.executeQuery();
            List<Comment> commentList = new ArrayList<>();
            while (resultSet.next()) {
                Comment comment = new Comment();
                comment.setId(resultSet.getLong(1));
                comment.setText(resultSet.getString(2));
                comment.setCreationDate(resultSet.getTimestamp(3));
                comment.setNewsId(resultSet.getLong(4));
                commentList.add(comment);
            }
            return commentList;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteNewsComment(Long newsId, Long commentId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (
                PreparedStatement preparedStatement =
                        connection.prepareStatement(DELETE_NEWS_FROM_COMMENTS)
        ) {
            preparedStatement.setLong(1, newsId);
            preparedStatement.setLong(2, commentId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }
}
