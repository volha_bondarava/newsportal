package com.epam.bondareva.dao.implementation;

import com.epam.bondareva.dao.IAuthorDao;
import com.epam.bondareva.entity.Author;
import com.epam.bondareva.exception.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
@Component
public class AuthorDao implements IAuthorDao {
    private String GET_AUTHOR_BY_ID = "SELECT AU_AUTHOR_ID_PK, AU_NAME FROM AUTHORS WHERE AU_AUTHOR_ID_PK = ?";
    private String INSERT_INTO_AUTHORS="INSERT INTO AUTHORS (AU_AUTHOR_ID_PK,AU_NAME) VALUES (SEQ_AUTHOR.NEXTVAL,?)";
    private String UPDATE_AUTHOR="UPDATE AUTHORS SET AU_NAME = ? WHERE AU_AUTHOR_ID_PK = ?";
    private String DELETE_AUTHOR ="DELETE FROM AUTHORS WHERE AU_AUTHOR_ID_PK = ?";
    private String GET_AUTHOR_LIST="SELECT AU_AUTHOR_ID_PK, AU_NAME FROM AUTHORS";
    private String INSERT_INTO_NEWS_AUTHORS="INSERT INTO NEWS_AUTHORS(NA_NEWS_AUTHOR_ID_PK,NA_AUTHOR_ID_FK,NA_NEWS_ID_FK) VALUES(SEQ_NEWS_AUTHOR.NEXTVAL,?,?)";
    private String DELETE_FROM_NEWS_AUTHORS="DELETE FROM NEWS_AUTHORS WHERE NA_AUTHOR_ID_FK = ? AND NA_NEWS_ID_FK=?";
    @Autowired
    private DataSource dataSource;

    public AuthorDao() {
    }

    public void setDataSource(DataSource dataSource) throws DaoException {
        this.dataSource = dataSource;
    }

    @Override
    public Long createEntity(Author author) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (
                PreparedStatement preparedStatement =
                        connection.prepareStatement(INSERT_INTO_AUTHORS, new String[]{"AU_AUTHOR_ID_PK"})
        ) {
            Long authorId = null;

            preparedStatement.setString(1, author.getName());

            if (preparedStatement.executeUpdate() > 0) {
                ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
                if (generatedKeys != null && generatedKeys.next()) {
                    authorId = generatedKeys.getLong(1);
                }
            }
            return authorId;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void updateEntity(Author author) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(UPDATE_AUTHOR)
        ) {
            preparedStatement.setString(1, author.getName());
            preparedStatement.setLong(2, author.getId());
            preparedStatement.executeQuery();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteEntity(Long authorId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(DELETE_AUTHOR)
        ) {
            preparedStatement.setLong(1, authorId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Author getEntityById(Long authorId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (
             PreparedStatement preparedStatement =
                     connection.prepareStatement(GET_AUTHOR_BY_ID)
        ) {
            preparedStatement.setLong(1, authorId);
            ResultSet resultSet = preparedStatement.executeQuery();
            Author author = null;
            if (resultSet.next()) {
                author = new Author();
                author.setId(resultSet.getInt(1));
                author.setName(resultSet.getString(2));
            }
            return author;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Author> getEntityList() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (Statement statement =
                     connection.createStatement()
        ) {
            ResultSet resultSet = statement.executeQuery(GET_AUTHOR_LIST);
            List<Author> authorList = new ArrayList<>();
            Author author;
            while (resultSet.next()) {
                author = new Author();
                author.setId(resultSet.getLong(1));
                author.setName(resultSet.getString(2));
                authorList.add(author);
            }
            return authorList;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }
    @Override
    public void addAuthorNews(Long authorId, Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (
                PreparedStatement preparedStatement =
                        connection.prepareStatement(INSERT_INTO_NEWS_AUTHORS)
        ) {
            preparedStatement.setLong(1, authorId);
            preparedStatement.setLong(2, newsId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteAuthorNews(Long authorId, Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (
                PreparedStatement preparedStatement =
                        connection.prepareStatement(DELETE_FROM_NEWS_AUTHORS)
        ) {
            preparedStatement.setLong(1, authorId);
            preparedStatement.setLong(2, newsId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }


}
