package com.epam.bondareva.dao;

import com.epam.bondareva.entity.Author;
import com.epam.bondareva.exception.DaoException;

/**
 * Created by Olya Bondareva on 29/03/2015.
 */
public interface IAuthorDao extends IDao<Author> {
    void addAuthorNews(Long authorId, Long newsId) throws DaoException;
    void deleteAuthorNews(Long authorId, Long newsId) throws DaoException;
}
