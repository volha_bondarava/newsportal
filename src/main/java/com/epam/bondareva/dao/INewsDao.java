package com.epam.bondareva.dao;

import com.epam.bondareva.entity.Author;
import com.epam.bondareva.entity.News;
import com.epam.bondareva.entity.Tag;
import com.epam.bondareva.exception.DaoException;

import java.util.List;

/**
 * Created by Olya Bondareva on 28/03/2015.
 */
public interface INewsDao extends IDao<News> {
    List<News> searchByTag(Tag tag) throws DaoException;
    List<News> searchByAuthor(Author author) throws DaoException;
    void addNewsTag(Long newsId, Long tagId) throws DaoException;
    void deleteNewsTag(Long newsId, Long tagId) throws DaoException;
}
