package com.epam.bondareva.dao;

import com.epam.bondareva.entity.Entity;
import com.epam.bondareva.exception.DaoException;

import java.util.List;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
public interface IDao<CurrentEntity extends Entity> {
    public Long createEntity (CurrentEntity entity) throws DaoException;
    public void updateEntity(CurrentEntity entity) throws DaoException;
    public void deleteEntity (Long id) throws DaoException;
    public CurrentEntity getEntityById (Long id) throws DaoException;
    public List<CurrentEntity> getEntityList() throws DaoException;
}
