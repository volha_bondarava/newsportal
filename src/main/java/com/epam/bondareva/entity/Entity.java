package com.epam.bondareva.entity;

import java.io.Serializable;

/**
 * Created by Olya Bondareva on 15/03/2015.
 */
public abstract class Entity implements Serializable{
    private static final long serialVersionUID = 4018135018505689380L;
    private Long id;

    public Entity() {
    }

    public Entity(Long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("id=").append(id);
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if((o == null) || (o.getClass() != this.getClass())) return false;

        Entity entity = (Entity) o;

        if (id != entity.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}