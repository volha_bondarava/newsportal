package com.epam.bondareva.entity;

import java.io.Serializable;

/**
 * Created by Olya Bondareva on 15/03/2015.
 */
public class Tag extends Entity implements Serializable{
    private static final long serialVersionUID = -294671987083948891L;
    private String name;

    public Tag(){
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if((o == null) || (o.getClass() != this.getClass())) return false;

        Tag tag = (Tag) o;

        if (!name.equals(tag.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("name=").append(name);
        return sb.toString();
    }
}
