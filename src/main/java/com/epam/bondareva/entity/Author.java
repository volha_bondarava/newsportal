package com.epam.bondareva.entity;

import java.io.Serializable;

/**
 * Created by Olya Bondareva on 15/03/2015.
 */
public class Author extends Entity implements Serializable{

    private static final long serialVersionUID = 1985180099758642417L;
    private String name;

    public Author() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Author)) return false;
        if (!super.equals(o)) return false;

        Author author = (Author) o;

        if (!name.equals(author.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("name=").append(name);
        return sb.toString();
    }
}
