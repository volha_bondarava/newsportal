package com.epam.bondareva.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Olya Bondareva on 15/03/2015.
 */
public class Comment extends Entity implements Serializable{
    private static final long serialVersionUID = -5723781317753794521L;
    private String text;
    private Date creationDate;
    private Long newsId;

    public Comment() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public long getNewsId() {
        return newsId;
    }

    public void setNewsId(long newsId) {
        this.newsId = newsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if((o == null) || (o.getClass() != this.getClass())) return false;

        Comment comment = (Comment) o;

        if (newsId != comment.newsId) return false;
        if (!creationDate.equals(comment.creationDate)) return false;
        if (!text.equals(comment.text)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = text.hashCode();
        result = 31 * result + creationDate.hashCode();
        result = 31 * result + (int) (newsId ^ (newsId >>> 32));
        return result;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("text=").append(text);
        sb.append("creationDate=").append(creationDate);
        sb.append("newsId=").append(newsId);
        return sb.toString();
    }
}
