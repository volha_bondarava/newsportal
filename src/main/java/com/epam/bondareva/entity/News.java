package com.epam.bondareva.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Olya Bondareva on 15/03/2015.
 */
public class News extends Entity implements Serializable{
    private static final long serialVersionUID = 3259166654133210563L;
    private String shortText;
    private String fullText;
    private String title;
    private Date creationDate;
    private Date modificationDate;

    public News() {
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if((o == null) || (o.getClass() != this.getClass())) return false;

        News news = (News) o;

        if (!creationDate.equals(news.creationDate)) return false;
        if (!fullText.equals(news.fullText)) return false;
        if (!modificationDate.equals(news.modificationDate)) return false;
        if (!shortText.equals(news.shortText)) return false;
        if (!title.equals(news.title)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = shortText.hashCode();
        result = 31 * result + fullText.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + creationDate.hashCode();
        result = 31 * result + modificationDate.hashCode();
        return result;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("shortText=").append(shortText);
        sb.append("fullText=").append(fullText);
        sb.append("title=").append(title);
        sb.append("creationDate=").append(creationDate);
        sb.append("modificationDate=").append( modificationDate);
        return sb.toString();
    }
}
