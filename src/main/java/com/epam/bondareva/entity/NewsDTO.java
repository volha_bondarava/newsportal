package com.epam.bondareva.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Olya Bondareva on 14/04/2015.
 */

public class NewsDTO implements Serializable{
    private News news;
    private Author author;
    private List<Tag> tagList;
    private List<Comment> commentList;

    public NewsDTO() {
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
