package com.epam.bondareva.exception;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
public class DaoException extends Exception {
    public DaoException() {
    }

    public DaoException(Throwable cause) {
        super(cause);
    }
}
