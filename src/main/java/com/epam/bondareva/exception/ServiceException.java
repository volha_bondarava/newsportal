package com.epam.bondareva.exception;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
public class ServiceException extends Exception {
    public ServiceException() {
    }

    public ServiceException(String cause) {
        super(cause);
    }
    public ServiceException(Throwable cause) {
        super(cause);
    }
}
