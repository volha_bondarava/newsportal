package com.epam.bondareva.dao;

import com.epam.bondareva.entity.Author;
import com.epam.bondareva.entity.News;
import com.epam.bondareva.entity.Tag;
import com.epam.bondareva.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import javax.sql.DataSource;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;

/**
* Created by Olya Bondareva on 07/04/2015.
*/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:spring-config-test.xml"})
@TestExecutionListeners(listeners = { DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class, TransactionalTestExecutionListener.class ,
        DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:sample/sample.xml")
public class NewsDaoTest {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private INewsDao newsDao;
    @Autowired
    private ICommentDao commentDao;
    @Autowired
    private IAuthorDao authorDao;

    @Test
    public void createEntity() throws DaoException {
        News news = new News();
        news.setId(11L);
        news.setTitle("title11");
        news.setShortText("shortText11");
        news.setFullText("fullText11");
        news.setModificationDate(new Timestamp(0));
        news.setCreationDate(new Timestamp(0));
        Long newsId = newsDao.createEntity(news);
        assertThat(newsId, greaterThan(0L));
    }

    @Test
    public void getEntityById() throws DaoException {
        Long newsId = 2L;
        News news = new News();
        news.setTitle("title2");
        news.setShortText("shortText2");
        news.setModificationDate(new Timestamp(0));
        news.setCreationDate(new Timestamp(0));
        news.setFullText("fullText2");
        news.setId(newsId);
        assertEquals(news, newsDao.getEntityById(newsId));
    }

    @Test
    @ExpectedDatabase(value = "classpath:expected/deleteNews.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void delete() throws DaoException {
        Long newsId = 2L;
        Long tagId = 2L;
        Long authorId = 2L;
        Long commentId = 2L;
        newsDao.deleteNewsTag(newsId, tagId);
        commentDao.deleteNewsComment(newsId, commentId);
        authorDao.deleteAuthorNews(authorId, newsId);
        newsDao.deleteEntity(newsId);
    }

    @Test
    @ExpectedDatabase(value = "classpath:expected/deleteNewsTag.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void deleteNewsTag() throws DaoException {
        Long tagId = 2L;
        Long newsId = 2L;
        newsDao.deleteNewsTag(newsId, tagId);
    }

    @Test
    public void getEntityList() throws DaoException {
        List<News> newsList = new ArrayList<>();
        int amount = 10;
        for(int i= 1; i <= amount; i++) {
            News news = new News();
            news.setId(i);
            news.setCreationDate(new Timestamp(0));
            news.setModificationDate(new Timestamp(0));
            news.setFullText("fullText" + i);
            news.setShortText("shortText" + i);
            news.setTitle("title" + i);
            newsList.add(news);
        }
        assertTrue(newsList.containsAll(newsDao.getEntityList()));
        assertEquals(newsList.size(), newsDao.getEntityList().size());
    }

    @Test
    public void searchByTag() throws DaoException {
        Tag tag = new Tag();
        tag.setId(2);
        tag.setName("tagName" + 2);

        News news = new News();
        news.setTitle("title2");
        news.setShortText("shortText2");
        news.setModificationDate(new Timestamp(0));
        news.setCreationDate(new Timestamp(0));
        news.setFullText("fullText2");
        news.setId(2L);
        List<News> newsList = new ArrayList<>();
        newsList.add(news);

        assertTrue(newsList.containsAll(newsDao.searchByTag(tag)));
        assertEquals(newsList.size(), newsDao.searchByTag(tag).size());
    }

    @Test
    public void searchByAuthor() throws DaoException {
        Author author= new Author();
        author.setId(2);
        author.setName("author" + 2);

        News news = new News();
        news.setTitle("title2");
        news.setShortText("shortText2");
        news.setModificationDate(new Timestamp(0));
        news.setCreationDate(new Timestamp(0));
        news.setFullText("fullText2");
        news.setId(2L);
        List<News> newsList = new ArrayList<>();
        newsList.add(news);

        assertTrue(newsList.containsAll(newsDao.searchByAuthor(author)));
        assertEquals(newsList.size(), newsDao.searchByAuthor(author).size());
    }
}
