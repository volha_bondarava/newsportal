package com.epam.bondareva.dao;

import com.epam.bondareva.entity.Comment;
import com.epam.bondareva.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import javax.sql.DataSource;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;

/**
* Created by Olya Bondareva on 07/04/2015.
*/

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:spring-config-test.xml"})
@TestExecutionListeners(listeners = { DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class, TransactionalTestExecutionListener.class ,
        DbUnitTestExecutionListener.class})
@DatabaseSetup(value = "classpath:sample/sample.xml")
public class CommentDaoTest {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private ICommentDao commentDao;

    @Test
    public void createEntity() throws DaoException {
        Comment comment = new Comment();
        comment.setId(11L);
        comment.setText("comment11");
        comment.setNewsId(1L);
        comment.setCreationDate(new Timestamp(0));
        Long commentId = commentDao.createEntity(comment);
        assertThat(commentId, greaterThan(0L));
    }

    @Test
    @ExpectedDatabase(value = "classpath:expected/deleteComment.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void delete() throws DaoException {
        Long commentId = 2L;
        commentDao.deleteEntity(commentId);
    }

    @Test
    @ExpectedDatabase(value = "classpath:expected/updateComment.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void update() throws DaoException {
        Comment comment = new Comment();
        comment.setText("commentText22");
        comment.setCreationDate(new Timestamp(0));
        comment.setNewsId(2L);
        comment.setId(2L);
        commentDao.updateEntity(comment);
    }

    @Test
    public void getEntityById() throws DaoException {
        Long commentId = 2L;
        Comment comment = new Comment();
        comment.setText("commentText2");
        comment.setNewsId(2);
        comment.setCreationDate(new Timestamp(0));
        comment.setId(commentId);
        assertEquals(comment, commentDao.getEntityById(commentId));
    }

    @Test
    public void getEntityListByNewsId() throws DaoException {
        List<Comment> commentList = new ArrayList<Comment>();
        Long newsId = 2L;
        Comment comment = new Comment();
        comment.setId(2);
        comment.setText("commentText2");
        comment.setNewsId(newsId);
        comment.setCreationDate(new Timestamp(0));
        commentList.add(comment);

        assertTrue(commentList.containsAll(commentDao.getEntityListByNewsId(newsId)));
        assertEquals(commentList.size(), commentDao.getEntityListByNewsId(newsId).size());
    }
}
