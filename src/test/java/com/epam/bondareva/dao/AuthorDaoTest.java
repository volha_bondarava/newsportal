package com.epam.bondareva.dao;

import com.epam.bondareva.entity.Author;
import com.epam.bondareva.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;

/**
 * Created by Olya Bondareva on 07/04/2015.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:spring-config-test.xml"})
@TestExecutionListeners(listeners = { DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class, TransactionalTestExecutionListener.class ,
        DbUnitTestExecutionListener.class})
@DatabaseSetup(value = "classpath:sample/sample.xml")
public class AuthorDaoTest {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private IAuthorDao authorDao;

    @Test
    @ExpectedDatabase(value = "classpath:expected/createAuthor.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void createEntity() throws DaoException {
        Author author = new Author();
        author.setName("author11");
        Long authorId = authorDao.createEntity(author);
        assertThat(authorId, greaterThan(0L));
    }

    @Test
    @ExpectedDatabase(value = "classpath:expected/deleteAuthor.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void delete() throws DaoException {
        System.out.println(authorDao.getEntityList().get(3).getId());
        Long authorId = 3L;
        authorDao.deleteEntity(authorId);
    }

    @Test
    @ExpectedDatabase(value = "classpath:expected/updateAuthor.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void update() throws DaoException {
        Author author = new Author();
        author.setId(4L);
        author.setName("author44");
        authorDao.updateEntity(author);
    }

    @Test
    public void getEntityById() throws DaoException {
        Long authorId = 5L;
        Author author = new Author();
        author.setName("author5");
        author.setId(authorId);
        assertEquals(author, authorDao.getEntityById(authorId));
    }

    @Test
    public void getEntityList() throws DaoException {
        List<Author> authorList = new ArrayList<>();
        int amount = 10;
        for(int i= 1; i <= amount; i++) {
            Author author = new Author();
            author.setId(i);
            author.setName("author"+i);
            authorList.add(author);
        }
            assertTrue(authorList.containsAll(authorDao.getEntityList()));
            assertEquals(authorList.size(), authorDao.getEntityList().size());
    }

//    @Test
//    @ExpectedDatabase(value = "classpath:expected/addAuthorNews.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
//    public void addAuthorNews() throws DaoException {
//        Long authorId = 11L;
//        Long newsId = 11L;
//        authorDao.addAuthorNews(authorId, newsId);
//    }

    @Test
    @ExpectedDatabase(value = "classpath:expected/deleteAuthorNews.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void deleteAuthorNews() throws DaoException {
        Long authorId = 2L;
        Long newsId = 2L;
        authorDao.deleteAuthorNews(authorId,newsId);
    }
}
