package com.epam.bondareva.service;

import com.epam.bondareva.dao.INewsDao;
import com.epam.bondareva.entity.Author;
import com.epam.bondareva.entity.News;
import com.epam.bondareva.entity.Tag;
import com.epam.bondareva.exception.ServiceException;
import com.epam.bondareva.service.implementation.NewsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
@ContextConfiguration(locations = {"classpath:spring-config-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class NewsServiceTest {
    @Mock
    private INewsDao newsDao;
    @InjectMocks
    private NewsService newsService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addNews() throws Exception {
        News news = new News();
        news.setId(1L);
        news.setFullText("fullTest1");
        news.setShortText("shortText1");
        news.setTitle("title1");
        news.setCreationDate(new Timestamp(0));
        news.setModificationDate(new Timestamp(0));

        when(newsDao.createEntity(news)).thenReturn(news.getId());
        assertEquals((Long)news.getId(), newsService.addNews(news));
        verify(newsDao, times(1)).createEntity(news);
        verifyNoMoreInteractions(newsDao);
    }

    @Test(expected=ServiceException.class)
    public void newsWasNotUpdated() throws Exception {
        News news = new News();
        news.setId(0L);
        news.setFullText("fullText1");
        news.setShortText("shortText1");
        news.setTitle("title1");
        news.setCreationDate(new Timestamp(0));
        news.setModificationDate(new Timestamp(0));

        when(newsDao.getEntityById(news.getId())).thenReturn(null);
        newsService.updateNews(news);
        verify(newsDao, times(1)).getEntityById(news.getId());
        verify(newsDao, times(0)).updateEntity(news);
        verifyNoMoreInteractions(newsDao);
    }


    @Test
    public void newsWasUpdated() throws Exception {
        News news = new News();
        news.setId(1L);
        news.setFullText("fullText1");
        news.setShortText("shortText1");
        news.setTitle("title1");
        news.setCreationDate(new Timestamp(200));
        news.setModificationDate(new Timestamp(200));

        when(newsDao.getEntityById(news.getId())).thenReturn(news);
        newsService.updateNews(news);
        verify(newsDao, times(1)).updateEntity(news);
        verify(newsDao, times(1)).getEntityById(news.getId());
        verifyNoMoreInteractions(newsDao);
    }

    @Test(expected=ServiceException.class)
    public void newsWasNotDeleted() throws Exception {
        Long newsId = 0L;
        when(newsDao.getEntityById(newsId)).thenReturn(null);
        newsService.deleteNews(newsId);
        verify(newsDao, times(0)).deleteEntity(newsId);
        verify(newsDao, times(1)).getEntityById(newsId);
        verifyNoMoreInteractions(newsDao);
    }

    @Test
    public void newsWasDeleted() throws Exception {
        News news = new News();
        news.setId(2L);
        news.setFullText("fullText1");
        news.setShortText("shortText1");
        news.setTitle("title1");
        news.setCreationDate(new Timestamp(200));
        news.setModificationDate(new Timestamp(200));

        when(newsDao.getEntityById(news.getId())).thenReturn(news);
        newsService.deleteNews(news.getId());
        verify(newsDao, times(1)).deleteEntity(news.getId());
        verify(newsDao, times(1)).getEntityById(news.getId());
        verifyNoMoreInteractions(newsDao);
    }

    @Test
    public void viewNewsList() throws Exception {
        newsService.viewNewsList();
        verify(newsDao, times(1)).getEntityList();
        verifyNoMoreInteractions(newsDao);
    }

    @Test
    public void viewSingleNews() throws Exception {
        Long newsId = 2L;
        newsService.viewSingleNews(newsId);
        verify(newsDao, times(1)).getEntityById(newsId);
        verifyNoMoreInteractions(newsDao);
    }

    @Test
    public void getNewsByAuthor() throws Exception {
        Author author = new Author();
        newsService.searchByAuthor(author);
        verify(newsDao, times(1)).searchByAuthor(author);
        verifyNoMoreInteractions(newsDao);
    }

    @Test
    public void getNewsByTag() throws Exception {
        Tag tag = new Tag();
        newsService.searchByTag(tag);
        verify(newsDao, times(1)).searchByTag(tag);
        verifyNoMoreInteractions(newsDao);
    }
}
