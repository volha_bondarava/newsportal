package com.epam.bondareva.service;

import com.epam.bondareva.dao.ICommentDao;
import com.epam.bondareva.entity.Comment;
import com.epam.bondareva.exception.ServiceException;
import com.epam.bondareva.service.implementation.CommentService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by Olya Bondareva on 03/04/2015.
 */
@ContextConfiguration(locations = {"classpath:spring-config-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class CommentServiceTest {
    @Mock
    private ICommentDao commentDao;
    @InjectMocks
    private CommentService commentService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addComment() throws Exception {
        Comment comment = new Comment();
        comment.setId(1L);
        comment.setText("comment1");
        comment.setCreationDate(new Timestamp(0));
        comment.setNewsId(1L);

        when(commentDao.createEntity(comment)).thenReturn(comment.getId());
        assertEquals(comment.getId(), commentService.addComment(comment));
        verify(commentDao, times(1)).createEntity(comment);
        verifyNoMoreInteractions(commentDao);
    }

    @Test(expected=ServiceException.class)
    public void commentWasNotDeleted() throws Exception {
        Long commentId = 0L;
        when(commentDao.getEntityById(commentId)).thenReturn(null);
        commentService.deleteComment(commentId);
        verify(commentDao, times(0)).deleteEntity(commentId);
        verify(commentDao, times(1)).getEntityById(commentId);
        verifyNoMoreInteractions(commentDao);
    }

    @Test
    public void commentWasDeleted() throws Exception {
        Long commentId = 1L;

        Comment comment = new Comment();
        comment.setId(1L);
        comment.setText("comment1");
        comment.setCreationDate(new Timestamp(0));
        comment.setNewsId(1L);

        when(commentDao.getEntityById(commentId)).thenReturn(comment);
        commentService.deleteComment(commentId);
        verify(commentDao, times(1)).deleteEntity(commentId);
        verify(commentDao, times(1)).getEntityById(commentId);
        verifyNoMoreInteractions(commentDao);
    }
}
