package com.epam.bondareva.service;

import com.epam.bondareva.dao.IAuthorDao;
import com.epam.bondareva.entity.Author;
import com.epam.bondareva.service.implementation.AuthorService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by Olya Bondareva on 03/04/2015.
 */
@ContextConfiguration(locations = {"classpath:spring-config-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class AuthorServiceTest {
    @Mock
    private IAuthorDao authorDao;
    @InjectMocks
    private AuthorService authorService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addAuthor() throws Exception {
        Author author = new Author();
        author.setId(2L);
        author.setName("author1");

        when(authorDao.createEntity(author)).thenReturn(author.getId());
        assertEquals(author.getId(), authorService.addAuthor(author));
        verify(authorDao, times(1)).createEntity(author);
        verifyNoMoreInteractions(authorDao);
    }

    @Test
    public void addAuthorNews() throws Exception {
        Long authorId = 2L;
        Long newsId = 3L;
        authorService.addAuthorNews(authorId, newsId);
        verify(authorDao, times(1)).addAuthorNews(authorId, newsId);
        verifyNoMoreInteractions(authorDao);
    }

    @Test
    public void deleteAuthorNews() throws Exception {
        Long authorId = 2L;
        Long newsId = 3L;
        authorService.deleteAuthorNews(authorId, newsId);
        verify(authorDao, times(1)).deleteAuthorNews(authorId, newsId);
        verifyNoMoreInteractions(authorDao);
    }
}
