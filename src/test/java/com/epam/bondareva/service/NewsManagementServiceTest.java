package com.epam.bondareva.service;

import com.epam.bondareva.entity.News;
import com.epam.bondareva.entity.Tag;
import com.epam.bondareva.service.implementation.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;

import static org.mockito.Mockito.*;

/**
* Created by Olya Bondareva on 25/03/2015.
*/
@ContextConfiguration(locations = {"classpath:spring-config-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class NewsManagementServiceTest {
    @InjectMocks
    private NewsManagementService newsManagementService;
    @Mock
    private INewsService newsService;
    @Mock
    private ICommentService commentService;
    @Mock
    private ITagService tagService;
    @Mock
    private IAuthorService authorService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addTagToNews() throws Exception {
        Long newsId = 2L;

        Tag tag=new Tag();
        tag.setId(2L);
        tag.setName("tag1");

        when(tagService.addTag(tag)).thenReturn(tag.getId());
        newsManagementService.addTagToNews(newsId, tag);

        when(tagService.addTag(tag)).thenReturn(tag.getId());
        verify(tagService, times(1)).addTag(tag);
        verify(newsService, times(1)).addNewsTag(newsId, tag.getId());
        verifyNoMoreInteractions(tagService, newsService);
    }

    @Test
    public void addNewsToAuthor() throws Exception {
        Long authorId = 2L;

        News news = new News();
        news.setId(2L);
        news.setFullText("fullTest1");
        news.setShortText("shortText1");
        news.setTitle("title1");
        news.setCreationDate(new Timestamp(0));
        news.setModificationDate(new Timestamp(0));

        when(newsService.addNews(news)).thenReturn(news.getId());
        newsManagementService.addNewsToAuthor(authorId, news);

        verify(authorService, times(1)).addAuthorNews(authorId, news.getId());
        verify(newsService, times(1)).addNews(news);
        verifyNoMoreInteractions(authorService, newsService);
    }



    @Test
    public void deleteNews() throws Exception {
        Long newsId = 2L;
        Long authorId = 2L;
        Long commentId = 2L;
        Long tagId = 2L;

        newsManagementService.deleteNewsFromAuthor(authorId, newsId);

        verify(tagService, times(1)).getTagListByNewsId(newsId);
        verify(commentService, times(1)).getCommentListByNewsId(newsId);
        verify(authorService, times(1)).deleteAuthorNews(authorId,newsId);
        verify(newsService, times(1)).deleteNews(newsId);
        verify(newsService, atLeast(0)).deleteNewsTag(newsId, tagId);
        verify(commentService, atLeast(0)).deleteComment(commentId);
        verify(commentService, atLeast(0)).deleteNewsComment(newsId, commentId);

        verifyNoMoreInteractions(authorService, newsService, commentService, tagService);
    }


}