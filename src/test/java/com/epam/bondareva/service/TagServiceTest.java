package com.epam.bondareva.service;

import com.epam.bondareva.dao.ITagDao;
import com.epam.bondareva.entity.Tag;
import com.epam.bondareva.exception.ServiceException;
import com.epam.bondareva.service.implementation.TagService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by Olya Bondareva on 03/04/2015.
 */
@ContextConfiguration(locations = {"classpath:spring-config-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class TagServiceTest {
    @Mock
    private ITagDao tagDao;
    @InjectMocks
    private TagService tagService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addTag() throws Exception {
        Tag tag = new Tag();
        tag.setId(2L);
        tag.setName("tag1");

        when(tagDao.createEntity(tag)).thenReturn(tag.getId());
        assertEquals(tag.getId(), tagService.addTag(tag));
        verify(tagDao, times(1)).createEntity(tag);
        verifyNoMoreInteractions(tagDao);
    }

    @Test(expected=ServiceException.class)
    public void tagWasNotDeletedTag() throws Exception {
        Long tagId = 2L;
        when(tagDao.getEntityById(tagId)).thenReturn(null);
        tagService.deleteTag(tagId);
        verify(tagDao, times(0)).deleteEntity(tagId);
        verify(tagDao, times(1)).getEntityById(tagId);
        verifyNoMoreInteractions(tagDao);
    }

    @Test
    public void newsWasDeleted() throws Exception {
        Tag tag = new Tag();
        tag.setId(1L);
        tag.setName("tag1");
        long tagId = 1;
        when(tagDao.getEntityById(tagId)).thenReturn(tag);
        tagService.deleteTag(tagId);
        verify(tagDao, times(1)).deleteEntity(tagId);
        verify(tagDao, times(1)).getEntityById(tagId);
        verifyNoMoreInteractions(tagDao);
    }
}
